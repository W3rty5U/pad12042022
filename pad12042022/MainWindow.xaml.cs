﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pad12042022
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Plant> plants;
        int activeItems = 0;

        public MainWindow()
        {
            InitializeComponent();
            plants = new List<Plant>();

            plants.Add(new Plant() { Name = "Bluszcz", ImageSource = new Uri(@"img\bluszcz.jpg", UriKind.Relative), Color="#5e81ac", StrokeColor="#b00b69" });
            plants.Add(new Plant() { Name = "Chryzantema", ImageSource = new Uri(@"img\chryzantema.jpg", UriKind.Relative), Color="#bf616a", StrokeColor="#69b00b" });
            plants.Add(new Plant() { Name = "Hiacynt", ImageSource = new Uri(@"img\hiacynt.jpg", UriKind.Relative), Color="#d08770", StrokeColor="#b00baa" });
            plants.Add(new Plant() { Name = "Kaktus", ImageSource = new Uri(@"img\kaktus.jpg", UriKind.Relative), Color="#ebcb8b", StrokeColor = "#bada55" });
            plants.Add(new Plant() { Name = "Lawenda", ImageSource = new Uri(@"img\lawenda.jpg", UriKind.Relative), Color="#a3be8c", StrokeColor = "#c0ff33" });
            plants.Add(new Plant() { Name = "Słonecznik", ImageSource = new Uri(@"img\slonecznik.jpg", UriKind.Relative), Color="#b48ead", StrokeColor = "#c43542" });

            MyListBox.ItemsSource = plants;

            MyProgressBar.Maximum = plants.Count;

            for(int i = 0; i < plants.Count; i++)
            {
                ((Rectangle)((Grid)MyUniformGrid.Children[i]).Children[0]).Fill = (Brush)(new BrushConverter().ConvertFrom(plants[i].Color));
                ((Rectangle)((Grid)MyUniformGrid.Children[i]).Children[0]).Stroke = (Brush)(new BrushConverter().ConvertFrom(plants[i].StrokeColor));
                ((Grid)MyUniformGrid.Children[i]).Visibility = Visibility.Hidden;
            }
        }

        private void onCheckboxChanged(object sender, RoutedEventArgs e)
        {
            int index = plants.FindIndex(x => x.Name == ((TextBlock)((StackPanel)((CheckBox)sender).Content).Children[1]).Text);

            if (((CheckBox)sender).IsChecked == true)
            {
                activeItems++;
                ((Grid)MyUniformGrid.Children[index]).Visibility = Visibility.Visible;
            }
            else
            {
                activeItems--;
                ((Grid)MyUniformGrid.Children[index]).Visibility = Visibility.Hidden;
            }

            MyProgressBar.Value = activeItems;
        }

        private void ScrollBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Grid grid = (Grid)((ScrollBar)sender).Parent;
            Rectangle rectangle = (Rectangle)grid.Children[0];
            rectangle.Height = 300 - ((ScrollBar)sender).Value;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Grid grid = (Grid)((Slider)sender).Parent;
            Rectangle rectangle = (Rectangle)grid.Children[0];
            rectangle.Width = ((Slider)sender).Value;
        }
    }

    public class Plant
    {
        public string Name { get; set; }
        public Uri ImageSource { get; set; }
        public string Color { get; set; }
        public string StrokeColor { get; set; }
    }
}
